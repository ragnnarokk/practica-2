Alejandro París Gómez

Hero Siege is a Slash Roguelike RPG by Panic Art Studios.

Hero Siege was created by Panic Art Studios and Auto Series Team Jam on September 2013 in 48 hours by Elias Viglione (Graphics), Jussi Kukkonen (Programming) and Aleksi Kujala (Composing). The final game was released 18th of December 2013.

On the July 18th 2014 the first DLC, called The Karp of Doom, was released, and adds the Samurai class, new Relics, a new Act Plains of Karponia, and a new difficulty level. Users that own this DLC will also have had access to an early Alpha for multiplayer.
Second DLC, The Depths of Hell, was released on the April 30th 2015. It adds new class Fallen Paladin, 6th act The Holy Grounds, new achievements, enemies, bosses, relics, dungeons, quests, siege mode, new clothing and exclusive loot.
The Amazon Jungle Bundle, released on the 8th of June 2015, adds new Amazon class, two new relics, dark green name color and Voodoo Mask hat.
The fourth DLC, The Wrath of Mevius, was released on the 17th of November 2015. It added two new new classes, The Demon Slayer and The Demonspawn. Along with a new act "The Rift", new relics, bosses, enemies and difficult challenges.

![baixa.jpg](https://bitbucket.org/repo/Kk4xpx/images/1792779412-baixa.jpg)


http://store.steampowered.com/app/269210/
